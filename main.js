console.log("running");
let products = [
  {
    name: "sneakers",
    tag: "shoe",
    price: 125,
    inCart: 0,
  },
];

console.table(products);
let hello = document.getElementById("click");

hello.addEventListener("click", () => {
  cartNumbers(products);
});

function onLoadCartNumbers() {
  let productNumbers = localStorage.getItem("cartNumbers");

  if (productNumbers) {
    document.querySelector(".count span").textContent = productNumbers;
  }
}

function cartNumbers(products) {
  // console.log("The product is clicked is", product);

  let productNumbers = localStorage.getItem("cartNumbers");
  productNumbers = parseInt(productNumbers);
  if (productNumbers) {
    localStorage.setItem("cartNumbers", productNumbers + 1);
    document.querySelector(".count span").textContent = productNumbers + 1;
  } else {
    localStorage.setItem("cartNumbers", 1);
    document.querySelector(".count span").textContent = 1;
  }

  setItems(products);
}
function setItems(product) {
  let cartItems = localStorage.getItem("productInCart");
  cartItems = JSON.parse(cartItems);
  if (cartItems != null) {
    cartItems[products.tag].inCart += 1;
  } else {
    product.inCart = 1;
    cartItems = {
      [products.tag]: products,
    };
  }
  console.log("My Cart items are", cartItems);

  localStorage.setItem("productInCart", JSON.stringify(cartItems));
}

onLoadCartNumbers();
